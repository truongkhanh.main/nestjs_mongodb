import { ApiProperty } from '@nestjs/swagger';
import { ArrayNotEmpty, IsArray, IsString } from 'class-validator';

export class DeleteFilesDto {
  @ApiProperty({
    name: 'fileKeys',
    type: Array,
    required: true,
    description: 'Array of strings can be convert to ObjectId',
    example: ['6358af7e76104848c0b7978c'],
  })
  @IsArray()
  @ArrayNotEmpty()
  @IsString({ each: true })
  fileKeys: string[];
}
