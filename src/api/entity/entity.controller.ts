import { Body, Controller, Delete, Get, Param, Patch, Post, UploadedFile, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBody, ApiConsumes } from '@nestjs/swagger';
import { ParseObjectIdPipe } from '../../share/pipe/parse-objectid.pipe';
import { CreateEntityDto, UploadFileDto, DeleteFilesDto } from './dto';
import { EntityService } from './entity.service';

@Controller('entity')
export class EntityController {
  constructor(private entityService: EntityService) {}

  @Get()
  getListEntity() {
    return this.entityService.getList();
  }

  @Get(':id')
  getEntityById(@Param('id', ParseObjectIdPipe) entityId: string) {
    return this.entityService.getById(entityId);
  }

  @Post()
  createEntity(@Body() body: CreateEntityDto) {
    return this.entityService.create(body);
  }

  @Get('file/:id')
  getFileUrl(@Param('id', ParseObjectIdPipe) fileId: string) {
    return this.entityService.getFileUrl(fileId);
  }

  @ApiConsumes('multipart/form-data')
  @ApiBody({
    description: 'Upload file',
    type: UploadFileDto,
  })
  @UseInterceptors(FileInterceptor('file'))
  @Post('file')
  upFile(@UploadedFile() file: Express.Multer.File) {
    return this.entityService.uploadFile(file);
  }

  @Delete('file/:id')
  deleteFile(@Param('id') fileKey: string) {
    return this.entityService.deleteFile(fileKey);
  }

  @Delete('files')
  deleteMultipleFiles(@Body() body: DeleteFilesDto) {
    return this.entityService.deleteMultipleFiles(body.fileKeys);
  }

  @Delete(':id/force-delete')
  deleteEntity(@Param('id', ParseObjectIdPipe) entityId: string) {
    return this.entityService.delete(entityId);
  }

  @Delete(':id')
  softDeleteEntity(@Param('id', ParseObjectIdPipe) entityId: string) {
    return this.entityService.softDelete(entityId);
  }

  @Patch(':id/restore')
  restore(@Param('id', ParseObjectIdPipe) entityId: string) {
    return this.entityService.restore(entityId);
  }
}
