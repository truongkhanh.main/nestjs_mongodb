import { ExternalServicesModule } from './../../share/external-services/external-services.module';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { EntityController } from './entity.controller';
import { EntityRepository } from './entity.repository';
import { EntityService } from './entity.service';
import { Entity, EntitySchema } from './schemas/entity.schema';
import { ClientsModule, Transport } from '@nestjs/microservices';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Entity.name, schema: EntitySchema }]),
    ExternalServicesModule,
    ClientsModule.register([
      {
        name: 'FILE_SERVICE',
        transport: Transport.RMQ,
        options: {
          urls: ['amqp://localhost:5672'],
          queue: 'FILE_QUEUE',
          queueOptions: {
            durable: true,
          },
        },
      },
    ]),
  ],
  controllers: [EntityController],
  providers: [EntityRepository, EntityService],
  exports: [EntityRepository, EntityService],
})
export class EntityModule {}
