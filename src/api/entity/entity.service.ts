import { AwsS3Service } from './../../share/external-services/s3.service';
import { EntityDocument } from './schemas/entity.schema';
import { CreateEntityDto } from './dto';
import { EntityRepository } from './entity.repository';
import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';

@Injectable()
export class EntityService {
  constructor(
    private entityRepository: EntityRepository,
    private s3Service: AwsS3Service,
    @Inject('FILE_SERVICE') private clientFileService: ClientProxy,
  ) {}

  create(data: CreateEntityDto) {
    return this.entityRepository.create(data as EntityDocument);
  }

  async getById(id: string) {
    try {
      return await this.entityRepository.findById(id);
    } catch (error) {
      throw error;
    }
  }

  getList() {
    return this.entityRepository.list({
      conditions: {},
      paginate: {
        page: 1,
        pageSize: 10,
      },
    });
  }

  delete(id: string) {
    return this.entityRepository.findByIdAndDelete(id);
  }

  softDelete(id: string) {
    return this.entityRepository.softDeleteById(id);
  }

  softDeleteByName(name: string) {
    return this.entityRepository.softDeleteByCondition({ name });
  }

  restore(id: string) {
    return this.entityRepository.restoreById(id);
  }

  restoreByName(name: string) {
    return this.entityRepository.restoreByCondition({ name });
  }

  async getFileUrl(id: string) {
    return this.clientFileService.send(`get-file-url`, { id });
  }

  uploadFile(file: Express.Multer.File) {
    return this.clientFileService.send('upload-file', { file });
  }

  deleteFile(id: string) {
    return this.clientFileService.send(`delete-single-file`, { id });
  }

  deleteMultipleFiles(fileKeys: string[]) {
    return this.clientFileService.send(`delete-multiple-files`, { ids: fileKeys });
  }
}
