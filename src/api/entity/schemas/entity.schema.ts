import { ENTITY_CONST } from './../entity.constant';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({ collection: ENTITY_CONST.MODEL_NAME, timestamps: true })
export class Entity {
  @Prop()
  name: string;
}

export type EntityDocument = Entity & Document;

export const EntitySchema = SchemaFactory.createForClass(Entity);
