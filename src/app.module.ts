import { MONGODB_CONFIG } from './configs/constant.config';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import * as MongooseDelete from 'mongoose-delete';
import { EntityModule } from './api/entity/entity.module';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    MongooseModule.forRoot(MONGODB_CONFIG.uri, {
      connectionFactory: (connection) => {
        // Add soft delete plugin to MongoDB connection
        connection.plugin(MongooseDelete, {
          deletedAt: true,
          overrideMethods: true,
        });
        return connection;
      },
    }),
    EntityModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
