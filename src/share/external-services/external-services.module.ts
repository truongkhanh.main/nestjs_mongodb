import { AwsS3Service } from './s3.service';
import { Module } from '@nestjs/common';

@Module({
  providers: [AwsS3Service],
  exports: [AwsS3Service],
})
export class ExternalServicesModule {}
